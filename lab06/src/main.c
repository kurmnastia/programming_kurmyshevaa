int main () {

#define N 2 // вводимо константу N, що буде дорівнювати розміру матриці, яку будемо множити

int ma [N][N] = {{2, 3}, {4, 5}}; // матриця ma, яка складається з N рядків та N стовбчиків
int mres [N][N] = {{0, 0}, {0, 0}}; // матриця mres, яка буде результатом множення матриці ma самої на себе

for (int row = 0; row < N; row++) {
	for (int col = 0; col < N; col++) {
		for (int i = 0; i < N; i++) { // відповідно до правил множення матриць "рядок на стовбчик"
			mres [row] [col] += ma [row][i] * ma [i][col];
			}
		}
	}



return 0;
}
